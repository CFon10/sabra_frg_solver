push!(LOAD_PATH, "/home/comef/Projects/SabraInverseRG/julia conversion")
include("solver.jl")
include("parameters.jl")
include("int.jl")
include("save.jl")


param = init_param(n_shell = 30, step_max=1e-1, step_min = 1e-2, s_max = 2e-1, eps = 10,
                     k0 = 1e-3, D = 1e-3, nu = 1e-10, beta = 1., alpha = 1., norm = 3E6, lamb = 2.,
                     n_save = 200, save_dir = "/home/comef/Projects/sabra_frg_solver/data/")
(lattice, f0)  = init_func(param)



save_struct(param.save_dir*"parameters.dat", param)
save_init(param.save_dir, f0, lattice)


Adaptative_Euler(get_rhs, f0, (0, param.s_max), param.step_max,
     adaptative_step_min = param.step_min, save_folder = param.save_dir,
     save_nb = param.n_save, args = (lattice, param))