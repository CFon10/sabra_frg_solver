This code was used to generate the data contained in the the following paper: https://doi.org/10.48550/arXiv.2208.00225
Feel free to use it to check the results of to build on it. 

Requirements:
Julia version 1.8 or above
CurveFit

To launch the computions, you need to execute the "main.jl" file with a Julia, or indirectly using the notebook.
parameters can be tuned through a Parameters struct, directly in the main file


This is a work project not meant published in an effort of transparency of its results, so its readability might not be optimal. Feel free to send me questions via email: come.fontaine (at) gmail.com