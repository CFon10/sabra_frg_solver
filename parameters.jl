



function save_struct(filename::AbstractString, s)
    io = open(filename, "w")
    dump(io, s)
    close(io)
end

function save_param(save_dir::AbstractString, param)
    save_struct(save_dir*"/parameters.dat", param)
end

struct Parameters
    n_shell::Int32
    k0::Float64
    lamb::Float64
    c::Float64
    nu::Float64
    D::Float64
    alpha::Float64
    beta::Float64
    eps::Float64
    norm::Float64
    pow::Float64

    step_max::Float64
    step_min::Float64
    s_max::Float64
    n_save::Int32

    save_dir::String
    direction::String
end

function init_param(; n_shell = 30, k0 = 1, lamb = 2., c = -0.5, nu = 1e-5, D = 1e-5, alpha = 1, beta = 1, eps = 1,
                        norm = 1., pow = 0.0, step_max = 1e-2, step_min = 1e-6, s_max = 30, n_save = 100, save_dir = "./", direction = "inverse")
    
    print("Parameters intialised correctly")
    return Parameters(n_shell, k0, lamb, c, nu, D, alpha, beta, eps, norm, pow, step_max, step_min, s_max, n_save, save_dir, direction)
end

function init_func(param)
    lattice = [param.k0*param.lamb^(i-1) for i in 1:param.n_shell]
    
    save_struct(param.save_dir*"/parameters.dat", param)

    f0 = vcat(
        param.nu*lattice.^(2),            #fnu    
        param.D*(param.eps.*((lattice./lattice[Int(floor(param.n_shell/3))]).^10 .|> tanh) .* (lattice./lattice[Int(floor(param.n_shell/3))]).^(param.pow) .+ 
        1 .*((lattice./param.k0).^(2)).*((-1e-2.*((lattice/param.k0).^2 .-4)).|>exp)), #fd
        lattice.^0,                      #flamb
        param.c .*lattice.^0, #C
        lattice.^0,
        -(1+param.c) .*lattice.^0, #B
        0 .* lattice,   #g111a
        0 .* lattice,   #g111b
        0 .* lattice,   #g12
        0 .* lattice,   #g03
        0 .* lattice,   #g22b
        0 .* lattice,   #g22a
        0 .* lattice,   #g31
        0 .* lattice,   #g13
        0 .* lattice    #g04

    )

    return (lattice, f0)
end

#((-(1E-8*lattice/param.k0).^2).|>exp).*(lattice.^2)
