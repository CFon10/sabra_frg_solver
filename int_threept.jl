include("reg.jl")

"""
These functions all compute the flow of the three point vertices. They
share the same prototype
These expression are the output of a Mathematica code make the formal
calculations from the Wetterich equation

Args:
i::int || index of the momenta
lattice::Arrays || lattice of momenta
fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, gamma22b, gamma22a, gamma31, 
gamma13, gamma04, dRk, dNk::Arrays || running function and regulator
param::Parameters
"""

function get_rhs_C(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)
    lamb = param.lamb
    real_i = i-2

    (-((b[i-1]*c[i]*dRk[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-2]^3*g111b[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2)) - 
    (b[i-1]*c[i]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-2]^3*g111b[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-
    2]^2*fNut[i-1]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]^2*fNut[i-1]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-2]^2*fNut[i-
    1]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]^2*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-2]*fNut[i-1]^
    2*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]^2*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-1]^3*g111b[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-1]^3*g111b[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^
    2*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-
    2]^2*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-2]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-2]^2*fNut[i]*
    g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (4*b[i-1]*c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i-1]*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fLambt[i-2]^3*fLambt[i-1]*fLambt[i]*fNut[i-1]^
    2*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-1]^2*fNut[i]*
    g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[i-
    1]^2*fNut[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i]*fLambt[i-1]^3*fNut[i-2]*g12[i-1]*lattice[i-1])/(
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fNut[i-1]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-1]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[i]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-2]*fLambt[i-1]^3*fNut[i]*g12[i-1]*lattice[i-1])/((
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] 
    + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*lattice[
    i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*lattice[
    i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^3*fNut[i-2]*lattice[i-
    1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fNut[i-2]*fNut[i]*
    lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[
    i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^3*fNut[i]*lattice[i-
    1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^3*fLambt[
    i]*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-1]^3*fLambt[i]^2*fNut[
    i-2]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[
    i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^3*
    fNut[i-2]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^3*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^3*
    fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[
    i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^3*
    fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^3*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]^2*fNut[
    i-1]^3*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^3*
    fNut[i-1]^3*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-
    2]^3*fNut[i-1]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    3*fNut[i-2]^2*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-
    2]^3*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[
    i-2]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^3*fNut[i-2]^2*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[
    i-2]^2*fNut[i-1]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[
    i-2]*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^3*fNut[i-2]*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-
    1]^3*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[
    i-1]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^3*fLambt[i]^2*
    fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]^2*fNut[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]^2*
    fNut[i-1]^2*fNut[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]^2*fNut[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]^2*
    fNut[i-1]^3*fNut[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i-1]*fNut[i]^2*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*fNut[i]^2*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-1]*fNut[i]^2*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]^2*fNut[i]^2*lattice[i-1]^2)/(fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i]^3*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i]^3*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i-1]*fNut[i]^3*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i-1]*c[i-1]*c[i]*dNk[i-1]*fLambt[i-1]*lattice[i-1]^2)/(2. *(
    fLambt[i-1]*fNut[i-2]*fNut[i-1] + 
    fLambt[i-2]*fNut[i-1]^2)*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (b[i-1]*c[i-1]*c[i]*dNk[i-2]*fLambt[i-1]*fLambt[i]*lattice[i-1]^2)/(
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*lattice[
    i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*lattice[
    i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i]*fLambt[i]*lattice[i-1]^2)/(2. *(fLambt[i]*
    fNut[i-1] + fLambt[i-1]*fNut[i])*(fLambt[i]*fNut[i-2]*fNut[i] + 
    fLambt[i-2]*fNut[i]^2)) - 
    (b[i]*b[i+1]*dRk[i+2]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[i]^3*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^3*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*fNut[i+1]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^2*fNut[i+1]*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]^2*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^3*fNut[i]*fNut[i+1]^2*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]*
    fNut[i+1]^2*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+2]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^3*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+1]^3*g111a[i+1]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[i]^2*fNut[i+2]*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (4*b[i]*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i+2]*fLambt[i]^3*fLambt[i+1]*fLambt[i+2]*fNut[i+1]^
    2*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^2*fNut[i+2]*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i+1]^2*
    fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*lattice[i+
    1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]*lattice[
    i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (3*b[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^3*fNut[i+1]*lattice[i+
    1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*
    lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^3*fNut[i+2]*lattice[i+
    1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]*
    lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fNut[i+1]*fNut[i+2]*
    lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (3*b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^3*fLambt[i+
    2]*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^
    2*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[i]^2*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^3*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[
    i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]^2*
    fNut[i]^3*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^3*
    fNut[i]^3*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+
    2]*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^3*
    fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^3*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*fLambt[i+2]
    *fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^3*fLambt[i+2]^2*fNut[
    i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^
    2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+
    1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^
    3*fNut[i+1]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]^2*fNut[i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^3*fNut[i]*
    fNut[i+1]^3*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[i]^
    3*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^
    3*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]^2*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]^2*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    ^2*fNut[i]*fNut[i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^3*fNut[i]*
    fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]*fNut[i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^
    3*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+1]^3*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]^2*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]^2*
    fNut[i]^3*fNut[i+2]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fLambt[
    i+2]*fNut[i]^2*fNut[i+2]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]^2*fNut[i+2]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[
    i]^2*fNut[i+2]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^3*fLambt[i+2]^2*
    fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]^2*fNut[
    i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fLambt[
    i+2]*fNut[i]^2*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^3*fNut[
    i]*fNut[i+2]^2*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fLambt[i+
    2]*fNut[i]*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^3*fNut[
    i]*fNut[i+2]^3*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]^2*fNut[
    i+2]^3*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+
    2]^3*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+2]*lattice[i+1]^2)/((
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]*c[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*c[i]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]^2*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*lattice[i+1]
    ^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]^2*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*lattice[i+1]
    ^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*c[i]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*c[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fNut[i]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*lattice[i]) - 
    (b[i+1]*c[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]*fNut[i]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*lattice[i]) - 
    (2*b[i+1]*c[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*lattice[i]) - 
    (b[i+1]*c[i+1]*dRk[i+2]*fLambt[i]^3*fNut[i+1]*g111a[i]*lattice[i+1]^2)
    /((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] 
    + fLambt[i]*fNut[i+2])^2*lattice[i]) - 
    (b[i+1]*c[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*lattice[i]) - 
    (b[i+1]*c[i+1]*dRk[i+1]*fLambt[i]^3*fNut[i+2]*g111a[i]*lattice[i+1]^2)
    /((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] 
    + fLambt[i]*fNut[i+2])^2*lattice[i]) - 
    (b[i+1]*c[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*lattice[i]))

end


function get_rhs_A(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)
    lamb = param.lamb
    real_i = i-2

    ((b[i-1]*b[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fNut[i-2]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]*fNut[i-2]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (2*b[i-1]*b[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i]*fLambt[i-2]^3*fNut[i-1]*g12[i-1]*lattice[i-1])/((
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fLambt[i-2]^3*fNut[i]*g12[i-1]*lattice[i-1])/((
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*lattice[i-1]^
    2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*lattice[i-1]^
    2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (3*b[i-1]^2*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^3*fNut[i-1]*lattice[i-1]^2)
    /(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*
    lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^3*fNut[i]*lattice[i-1]^2)
    /(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*
    lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (b[i-1]^2*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fNut[i-1]*fNut[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]^2*b[i]*dNk[i-2]*fLambt[i-2]*lattice[i-1]^2)/(2. *fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])) - 
    (3*a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^3*fLambt[
    i]*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[
    i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^3*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^3*
    fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]^2*fNut[
    i-2]^3*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^3*
    fNut[i-2]^3*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^3*
    fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^3*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^3*fLambt[i]^2*fNut[
    i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[
    i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^3*
    fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-
    2]^3*fNut[i-1]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    3*fNut[i-2]^2*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-
    2]^3*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[
    i-2]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^3*fNut[i-2]^2*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[
    i-2]^2*fNut[i-1]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[
    i-2]*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^3*fNut[i-2]*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-
    1]^3*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[
    i-1]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]^2*
    fNut[i-2]^3*fNut[i]*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]^2*fNut[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*fNut[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]^2*
    fNut[i-2]^2*fNut[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^3*fLambt[i]^2*
    fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]^2*fNut[i]^2*lattice[i-1]^2)/(fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i-2]*fNut[i]^2*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i]^2*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]*fNut[i]^2*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i-2]*fNut[i]^3*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i]^3*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i]^3*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i-1]*b[i-1]*b[i]*dNk[i-1]*fLambt[i-2]*fLambt[i]*lattice[i-1]^2)/(
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*b[i]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*lattice[
    i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*a[i]*b[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*lattice[i-
    1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*a[i]*b[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*lattice[i-
    1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*b[i]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*lattice[
    i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*dRk[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-2]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-2]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (3*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-
    2]^2*fNut[i-1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*b[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]^2*fNut[i-1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-2]^2*fNut[i-
    1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (3*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]^2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-2]*fNut[i-1]^
    2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]^2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-1]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-1]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^
    2*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-
    2]^2*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-2]^2*fNut[i]*
    g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (4*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]^3*fLambt[i-1]*fLambt[i]*fNut[i-1]^
    2*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-1]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-1]^2*fNut[i]*
    g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[i-
    1]^2*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) - 
    (a[i]*a[i+1]*dRk[i+2]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[i]^3*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^3*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    fNut[i+1]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[i]^2*fNut[i+1]*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[i]^2*fNut[i+2]*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (4*a[i]*a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^3*fNut[i]*fNut[i+2]^2*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]^2*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]*
    fNut[i+2]^2*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+1]*fNut[i+2]^2*
    g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fLambt[i]^3*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]^2*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]^2*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+2]^3*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+2]^3*g111a[i+1]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^3*fNut[i]*fNut[i+1]^2*
    g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]*
    fNut[i+1]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^3*g111b[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+1]^3*g111b[i+1]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (4*a[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+2]*fLambt[i]^3*fLambt[i+1]*fLambt[i+2]*fNut[i+1]^
    2*fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^2*fNut[i+2]*
    g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]^2*fNut[i+1]^
    2*fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^3*fNut[i]*fNut[i+2]^2*
    g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]*
    fNut[i+2]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+1]*fNut[i+2]^2*
    g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fLambt[i]^3*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+2]^3*g111b[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+2]^3*g111b[i+1]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^3*
    fLambt[i+2]*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]^2*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[
    i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^3*fNut[i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[
    i]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]^2*
    fNut[i]^3*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^3*
    fNut[i]^3*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]^2*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]^2*fNut[
    i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+
    2]^2*fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+
    1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    3*fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+
    1]^3*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+1]^
    3*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^3*
    fNut[i]^3*fNut[i+1]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^3*fNut[i]^2*fNut[i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^3*fNut[
    i]*fNut[i+1]^3*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]^2*
    fNut[i]^3*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^3*
    fNut[i]^3*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]^2*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^3*fNut[i]^2*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^3*fNut[
    i]^2*fNut[i+1]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*fNut[i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^3*fNut[
    i]*fNut[i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    3*fNut[i]*fNut[i+1]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]^2*fNut[
    i+1]^3*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^3*fNut[i+
    1]^3*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^3*
    fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^3*fLambt[
    i+2]*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+
    2]^2*fNut[i]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]^2*
    fNut[i]^3*fNut[i+2]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fLambt[
    i+2]*fNut[i]^2*fNut[i+2]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]^2*fNut[i+2]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]^2*fNut[
    i]^2*fNut[i+2]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]^2*
    fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+
    2]^2*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+
    2]^2*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fLambt[i+
    2]*fNut[i+1]^2*fNut[i+2]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+
    1]^2*fNut[i+2]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+
    2]^2*fNut[i+1]^2*fNut[i+2]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]^2*fNut[i+1]^
    3*fNut[i+2]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]^2*fNut[
    i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]^2*fNut[
    i+2]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+
    2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+
    2]^2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^3*fLambt[i+
    2]*fNut[i+2]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fLambt[
    i+2]*fNut[i]^2*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^3*fNut[
    i]*fNut[i+2]^2*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fLambt[i+
    2]*fNut[i]*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+
    1]*fNut[i+2]^2*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fLambt[i+
    2]*fNut[i+1]*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+1]*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]^2*fNut[i+2]^2*lattice[i+1]^2)/(fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+
    2]^3*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+2]^
    3*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^3*fNut[
    i]*fNut[i+2]^3*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]^2*fNut[
    i+2]^3*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^3*fNut[i+
    2]^3*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]^2*fNut[i+1]*
    fNut[i+2]^3*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+1]*lattice[i+1]^2)/((
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*c[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])))  

end


function get_rhs_B(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)
    lamb = param.lamb
    real_i = i-2

    (-((a[i]*c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fNut[i-2]*g111b[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2)) - 
    (a[i]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]*fNut[i-2]*g111b[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (2*a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i]*fLambt[i-2]^3*fNut[i-1]*g111b[i-1]*lattice[i-1])/(
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*g111b[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-1]*fLambt[i-2]^3*fNut[i]*g111b[i-1]*lattice[i-1])/(
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*g111b[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*lattice[i-1]^
    2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*lattice[i-1]^
    2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (3*a[i]*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^3*fNut[i-1]*lattice[i-1]^2)
    /(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*
    lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^3*fNut[i]*lattice[i-1]^2)
    /(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*
    lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (a[i]*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fNut[i-1]*fNut[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) - 
    (5*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^3*fLambt[
    i]*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[
    i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^3*fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^3*
    fNut[i-2]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]^2*fNut[
    i-2]^3*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^3*
    fNut[i-2]^3*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[
    i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-
    2]^3*fNut[i-1]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    3*fNut[i-2]^2*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-
    2]^3*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[
    i-2]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^3*fNut[i-2]^2*fNut[i-1]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^3*fNut[
    i-2]^2*fNut[i-1]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[
    i-2]*fNut[i-1]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^3*fNut[i-2]*fNut[i-1]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i]^2*fNut[i-
    1]^3*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^3*fNut[
    i-1]^3*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]^2*
    fNut[i-2]^3*fNut[i]*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]^2*fNut[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]^2*fNut[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]^2*
    fNut[i-2]^2*fNut[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-1]*fNut[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i]^2*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]*
    fLambt[i]*fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i]^2*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]^2*fNut[i]^2*lattice[i-1]^2)/(fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i-2]*fNut[i]^2*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*fNut[i]^2*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]*fNut[i]^2*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i-2]*fNut[i]^3*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]^2*
    fNut[i]^3*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^3*
    fNut[i]^3*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i-1]*b[i]*c[i-1]*dNk[i-1]*fLambt[i-1]*lattice[i-1]^2)/(2. *fNut[i-
    1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*a[i]*c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i-1]*lattice[i-1]^2)/((
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]^2*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*lattice[i-1]^
    2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*a[i]*c[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*lattice[i-
    1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*a[i]*c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*lattice[i-
    1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]^2*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*lattice[i-1]^
    2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*dRk[i]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-2]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-2]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*fNut[i-1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]^2*fNut[i-1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^3*fNut[i-2]^2*fNut[i-
    1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*c[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^
    2*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (3*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]^2*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-1]^3*fLambt[i]^2*fNut[i-2]^2*fNut[i]*
    g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (4*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^3*fNut[i-2]*fNut[i]^2*
    g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (3*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*fNut[i]^2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^3*fLambt[i]*fNut[i-
    2]*fNut[i]^2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i]*fLambt[i-2]^3*fLambt[i-1]^2*fNut[i-1]*fNut[i]^2*
    g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]^3*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]^2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fLambt[i]*fNut[i-
    1]*fNut[i]^2*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]^3*fLambt[i-1]^2*fNut[i]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) + 
    (a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^3*fNut[i]^3*g12[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2*lattice[i]) - 
    (a[i+1]*c[i]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fLambt[i+2]^2*g111b[i+1]*lattice[i+1])/((fLambt[
    i+2]*fNut[i] + fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i]*fLambt[i+2]^2*g111b[i+1]*lattice[i+1])/((fLambt[i+
    2]*fNut[i] + fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*lattice[i+1]^2)/(2. *
    fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] - fLambt[i]*fNut[i+2])) + 
    (a[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*lattice[i+1]^2)/(4. *fNut[
    i]^2*(fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] 
    - fLambt[i]*fNut[i+2])) + 
    (a[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*lattice[i+1]^2)/(4. *fNut[
    i]^2*(fLambt[i+1]*fNut[i] - fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] 
    + fLambt[i]*fNut[i+2])) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*lattice[
    i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*b[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*lattice[
    i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^3*fNut[i]*lattice[i+
    1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fNut[i]*fNut[i+2]*
    lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i+
    2]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^3*fNut[i+2]*lattice[i+
    1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]
    *fNut[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[
    i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+1]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+
    1]^2*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[
    i+1]^2*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i]*fNut[i+1]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]^2*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+
    2]*fNut[i+1]*fNut[i+2]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+
    2]^2*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]^2*c[i]*dRk[i+1]*fDt[i+2]*fLambt[i+2]^2*lattice[i+1]^2)/(2. *
    fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i+1]^3*lattice[i+1]^2)/((
    fLambt[i+1]*fNut[i] - fLambt[i]*fNut[i+1])*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]^2*c[i]*dRk[i]*fDt[i+2]*fLambt[i+2]^2*lattice[i+1]^2)/(2. *fNut[
    i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i+2]^3*lattice[i+1]^2)/((
    fLambt[i+2]*fNut[i] - fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i]*c[i+1]*dNk[i]*fLambt[i+1]*fLambt[i+2]*lattice[i+1]^2)/((
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*c[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]^2*c[i]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*lattice[i+1]
    ^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]^2*c[i]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*lattice[i+1]
    ^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*c[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i+2]^3*lattice[i+1]^2)/((
    fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*(fLambt[i+2]^2*fNut[i]^2 - 
    fLambt[i]^2*fNut[i+2]^2)) - 
    (a[i+1]*c[i+1]*dRk[i+2]*fLambt[i+1]^3*fNut[i]*g111a[i]*lattice[i+1]^2)
    /((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]) - 
    (a[i+1]*c[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fNut[i+1]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]) - 
    (2*a[i+1]*c[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]) - 
    (a[i+1]*c[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i+1]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i+2]*g111a[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]) - 
    (a[i+1]*c[i+1]*dRk[i]*fLambt[i+1]^3*fNut[i+2]*g111a[i]*lattice[i+1]^2)
    /((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2*lattice[i]))

end


function get_rhs_g111a(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)
    
    (-((c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2)) - 
    (2*c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[i]
    *g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111b[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111b[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111b[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i]*fLambt[i-1]^2*g03[i]*lattice[i-1]^2)/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*g111a[i]*lattice[i-1]^2)/
    (2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]
    ^2*fNut[i-2]*g111a[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]
    *fNut[i]*g111a[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]
    *fNut[i-2]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i-1]*c[i-1]*dNk[i-1]*fLambt[i-1]*g111a[i]*lattice[i-1]^2)/(2. *fNut[
    i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*dNk[i-2]*fLambt[i-2]*g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])) - 
    (3*a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (7*a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[
    i-1]^2*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*
    g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*fNut[i]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*g111b[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*
    g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*fNut[i]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*
    g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111b[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*fNut[
    i]*g111b[i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111b[i]*lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*fNut[
    i]^2*g111b[i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*g111b[i]*lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]^2*g111b[
    i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i-1]*g111b[i]*lattice[i-1]^2)
    /((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i]*
    lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111b[i]*
    lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fNut[i]*g111b[i]
    *lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i-1]*fNut[
    i]*g111b[i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]*g111b[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g111a[i-1]*g111b[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-1]*g111b[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[i]
    *g111a[i-1]*g111b[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[i]
    *g111a[i-1]*g111b[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i-1]*fLambt[i]^2*g111a[i-1]*g111b[i-1]*lattice[i])/((
    fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]*g111b[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g111a[i-1]*g111b[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-1]*g111b[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[i]*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i-1]*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i-2]*fLambt[i-1]^2*g03[i-1]*lattice[i-1]*lattice[i]
    )/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-1]^4*fLambt[i]*fNut[i-2]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] - 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (3*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fLambt[i]
    *fNut[i-1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] - fLambt[i-2]*fNut[i-1])^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^3*fNut[i-1]
    *fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    - fLambt[i-2]*fNut[i-1])^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (5*a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*
    g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[
    i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-
    1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^2*fNut[i-1]^2*g111a[
    i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i]^2*g111a[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*g111a[i-
    1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-1]
    *g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*
    g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (7*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-2]*fLambt[i-1]*fLambt[i]*g111a[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i]*dNk[i]*fLambt[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *
    fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fNut[i]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i-1]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*g111a[i-1]*lattice[i-1]*
    lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] - 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2]^2 + 
    fLambt[i-2]*fNut[i-2]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (4*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (4*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (4*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*g111b[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (c[i-1]*c[i]*dNk[i-1]*fLambt[i-1]*g111b[i-1]*lattice[i-1]*lattice[i]
    )/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fLambt[i-2]*fLambt[i]*g111b[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111b[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    *fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    2*fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]^3*
    fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]^3*
    fNut[i-2]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]
    ^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[
    i-1]*fLambt[i]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^3*fLambt[i]*
    fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    ^2*fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2]
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2]
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]^2*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    3*fNut[i-2]*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*
    fNut[i-2]*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fNut[i]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*lattice[i-1]^2*lattice[i])/(fNut[i-2]*fNut[i-1]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    *fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    2*fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i]*
    fNut[i-1]*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]*
    fNut[i]^2*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*c[i]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i]*lattice[i-1]^2*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*c[i]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i]*lattice[i-1]^2*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*c[i]*dNk[i-1]*fDt[i-2]*fLambt[i-1]*fLambt[i]*lattice[i-1]^2*
    lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*c[i]*dNk[i-2]*fDt[i-1]*fLambt[i-1]*fLambt[i]*lattice[i-1]^2*
    lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i]*lattice[i-1]
    ^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]*lattice[i-1]
    ^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*c[i]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]^2*c[i]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i]*g111a[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g111a[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i]*g111a[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i]*g111a[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g111a[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g111a[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g111a[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i]*g111a[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i]*g111a[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g111a[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i]*g111a[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i+2]*fLambt[i]^2*g111a[i]*g111b[i+1]*lattice[i+1])/((
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g111a[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g111a[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g111a[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g111a[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i+2]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[i+1]*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i+2]*
    g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fLambt[i+2]^2*g03[i+1]*lattice[i]*lattice[i+1])/
    ((fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i]*fLambt[i+2]^2*g03[i+1]*lattice[i]*lattice[i+1])/((
    fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*b[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[
    i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+
    2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i]*dNk[i+1]*fLambt[i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/
    (2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i+1]*fLambt[i+2]^2*g111a[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+2]^2*fNut[i+1]*g111a[
    i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (7*a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+
    1]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+
    2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i+
    2]*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*fNut[i+2]
    *g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]
    ^2*g111a[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]^2*g111a[
    i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i]*dNk[i]*fLambt[i+1]*fLambt[i+2]*g111a[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*c[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*g111b[i+1]*lattice[i]*
    lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^3*g111b[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]^2*fNut[i]^2 - 
    fLambt[i]^2*fNut[i+1]^2)*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2) + 
    (b[i+1]*c[i]*dNk[i]*fLambt[i]*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *
    fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*g111b[i+1]*lattice[i]*
    lattice[i+1])/(2. *(fLambt[i+1]*fNut[i]*fNut[i+1] - 
    fLambt[i]*fNut[i+1]^2)*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+2]^2*g111b[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (7*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[
    i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+
    2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+2]^2*fNut[i+1]*g111b[i+
    1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+
    2]*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i+2]
    *g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*fNut[i+2]
    *g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]
    ^2*g111b[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]^2*g111b[
    i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i]*b[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+2]*g111b[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+2]^3*fNut[i]*fNut[i+1]*g12[i+
    1]*lattice[i]*lattice[i+1])/(4. *fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+
    1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+
    1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*g12[
    i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[i+1]*g12[
    i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+2]^2*g12[i+1]*lattice[i]*
    lattice[i+1])/(2. *fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i]*dRk[i]*fDt[i+2]*fLambt[i+2]^2*g12[i+1]*lattice[i]*
    lattice[i+1])/(2. *fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i]*b[i+1]*dNk[i+2]*fLambt[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(
    2. *fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*c[i]*dNk[i+2]*fLambt[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/
    (2. *fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (5*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+
    2]*fNut[i]*g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+1]*g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[
    i+1]^2*g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i]*fNut[i+1]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]
    *g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+1]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g111a[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+1]*g111a[i]*lattice[i+1]^2)
    /((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111a[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111a[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (2*a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[
    i+1]*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^
    2*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]
    ^2*fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]
    *fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]
    ^3*fNut[i]^2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]^
    3*fNut[i]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^
    3*fNut[i]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*
    fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]
    *fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+
    2]^2*fNut[i]^2*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]
    ^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]
    ^2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+
    1]*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+
    2]*fNut[i]^2*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]
    ^2*fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+
    2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+
    2]^2*fNut[i]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]
    ^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]^
    2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]
    ^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^
    3*fNut[i]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+
    1]^2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^
    2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]*
    fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i]
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^
    2*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*
    lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*
    fNut[i+1]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*c[i]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*c[i]*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*lattice[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*c[i]*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*lattice[i]
    *lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*b[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*lattice[
    i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*b[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*lattice[
    i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*b[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*b[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*c[i]*dNk[i+2]*fDt[i]*fLambt[i+1]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*c[i]*dNk[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*b[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[
    i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i]*b[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[
    i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*c[i]*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*c[i]*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])))
end


function get_rhs_g111b(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)

    ((b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i]*g12[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g111a[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i]*g12[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i]*g12[
    i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g111a[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i]*g12[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g111a[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i]*g12[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g111a[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g111a[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i]*g12[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i-1]*fLambt[i]^2*g03[i]*lattice[i-1]^2)/((fLambt[
    i]*fNut[i-2] + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]
    *fNut[i]*g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]
    ^2*fNut[i-2]*g111a[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i]^2*g111a[i]*
    lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (7*a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]
    *fNut[i]*g111a[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    fNut[i]*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]
    ^2*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[i]^2*
    g111a[i]*lattice[i-1]^2)/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i-1]*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-2]*g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111a[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i]*lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i]^2*fNut[i-1]*g111a[i]
    *lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111a[i]*lattice[i-1]^2)/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*fNut[
    i]*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i]*
    g111a[i]*lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*fNut[i]*
    g111a[i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i]*lattice[i-1]^2)/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*fNut[
    i]^2*g111a[i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]^2*g111a[
    i]*lattice[i-1]^2)/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*dNk[i-1]*fLambt[i-2]*fLambt[i]*g111a[i]*lattice[i-1]^2)
    /((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111a[i]*
    lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i]*lattice[i-
    1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i]*lattice[i-
    1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i]*
    lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*
    fNut[i-1]*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[
    i-1]^2*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i]^2*g111b[i]*lattice[i-1]^2)/
    (2. *fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i]*
    lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111b[i]*
    lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dRk[i]*fLambt[i-1]^2*g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) + 
    (c[i]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]*g12[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[i]
    *g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[i]
    *g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-1]*g12[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]*g12[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*c[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[i]
    *g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-1]*g12[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g111a[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g111a[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g111a[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g111a[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]*
    g111a[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i-1]*c[i]*dRk[i]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fLambt[i-2]*fLambt[i]^2*fNut[i-1]*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i]*g03[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i-1]
    *lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-2]*fLambt[i]^2*g03[i-1]*lattice[i-1]*lattice[i])/((
    fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*g111a[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] - 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])^2) 
    - (a[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*g111a[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fLambt[i-1]^3*g111a[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] - 
    fLambt[i-2]*fNut[i-1])*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*
    g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[
    i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*
    g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]*fLambt[i]^2*g111a[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i]^2*fNut[i-1]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (7*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*fNut[
    i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-2]*fLambt[i-1]*fLambt[i]*g111a[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*c[i]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (4*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (4*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-
    1]^2*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (4*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*
    g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g12[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i]^2*g12[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g12[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dNk[i]*fLambt[i-2]*fLambt[i-1]*g12[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g12[i-1]*lattice[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g12[i-1]*lattice[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    *fLambt[i]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    ^2*fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]^3*
    fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]^3*
    fNut[i-2]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[
    i-2]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[
    i-1]^2*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i-1]*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[
    i-1]*fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^3*fLambt[i]*
    fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    ^2*fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]^2*
    lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]^2*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    3*fNut[i-2]*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[
    i-1]^2*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    2*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    *fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    2*fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i]*fNut[
    i-1]^2*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]*fNut[
    i-1]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]^2*
    lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]^2*fNut[
    i-1]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i-1]*b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fNut[i-1]*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]^2*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*c[i]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*lattice[i-1]^2*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*c[i]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*lattice[i-1]^2*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i]*lattice[i-1]
    ^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]*lattice[i-1]
    ^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*c[i]*dNk[i]*fDt[i-2]*fLambt[i-1]*fLambt[i]*lattice[i-1]^2*
    lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*c[i]*dNk[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]*lattice[i-1]^2*
    lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i-1]*b[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*c[i]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]^2*c[i]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i+1]*g12[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g111a[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+1]*g12[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g111a[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g111a[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i+1]*g12[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g111a[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+1]*g12[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i]*fLambt[i+2]^2*g111a[i+1]*g12[i]*lattice[i+1])/((fLambt[
    i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i+2]*fLambt[i]^2*g03[i+1]*lattice[i]*lattice[i+1])/(
    (fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*g111a[i+1]*lattice[i]*
    lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (4*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (4*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (4*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+
    2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]^2*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]
    ^2*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]^2*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dNk[i]*fLambt[i+1]*fLambt[i+2]*g111a[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (7*a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]
    *fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+
    1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+
    2]^2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*g12[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*g12[
    i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+1]*g12[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*c[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i]*fNut[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (2*b[i+1]*c[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i+1]*fLambt[i]^2*g03[i]*lattice[i+1]^2)/((fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])) - 
    (b[i+1]*c[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*c[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+
    1]*fNut[i+2]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*g111b[
    i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fNut[i+1]*fNut[i+2]*g111b[
    i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])^2) 
    - (b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*g111b[i]*lattice[i+1]^2)/
    (2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])) - 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[
    i+2]*g111b[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+
    2]^2*g111b[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111b[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (5*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*fNut[
    i+1]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+1]^
    2*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+2]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (7*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]
    ^2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+2]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[
    i+2]^2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]^2*g12[i]
    *lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[i]*fNut[i+1]*
    g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]*g12[i]*
    lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]*g12[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]^
    2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (3*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]
    *fNut[i]*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+2]*g12[i]*lattice[i+1]^2)
    /((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*c[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]^2*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i]*lattice[i+
    1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]^2*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i]*lattice[i+
    1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i]*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (3*b[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fNut[i+2]
    *g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*c[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g12[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i+2]^2*g12[i]*lattice[i+1]^2)/(2. *(
    fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1]*fNut[i+2] + 
    fLambt[i+1]*fNut[i+2]^2)) - 
    (3*a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[
    i+1]^2*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+
    2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]^2*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]
    *fNut[i]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^
    2*fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*
    fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^
    2*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]*fNut[i+
    1]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[
    i+1]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]^2*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]*fNut[
    i+1]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]^2*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*fNut[i+
    1]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*
    fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(fNut[i]*fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+
    2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]^2*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*lattice[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]^2*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*lattice[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+2]*lattice[i]
    *lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+2]*lattice[i]
    *lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i+1]*fDt[i]*fLambt[i+1]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]^2*dNk[i+2]*fDt[i]*fLambt[i+1]*fLambt[i+2]*lattice[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]^2*dNk[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*lattice[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]^2*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]^2*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^
    2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]
    *fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*c[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])))
end


function get_rhs_g12(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)


    (-((b[i-1]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i-1]*
    g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2)) - 
    (2*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g111b[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-1]*g12[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g111b[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g111b[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i-1]*
    g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g111b[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-1]*g12[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-2]*fLambt[i]^2*g111b[i-1]*g12[i]*lattice[i-1])/((fLambt[
    i]*fNut[i-2] + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g12[i-1]*g12[i]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g12[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g12[i-1]*g12[i]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g12[i-1]*g12[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g12[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g12[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g12[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g12[i-1]*g12[i]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g12[i-1]*g12[i]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g12[i-1]*g12[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g12[i-1]*g12[i]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*
    g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g12[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]
    *fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g12[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]
    *g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-1]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g12[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g12[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]
    *g12[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-1]*g12[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g12[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]
    *fNut[i]*g12[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]
    *fNut[i-1]*fNut[i]*g12[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*
    fNut[i]*g12[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g12[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*
    fNut[i]^2*g12[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fLambt[i-1]*fLambt[i]*g12[i]*lattice[i-1]^2)/(
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g12[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g12[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g111b[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g111b[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]*
    g111b[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]*
    g111b[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+2]
    *g111b[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*b[i-1]*b[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-2]*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i]*fLambt[i-2]^2*g03[i-1]*lattice[i-1]*lattice[i])/((
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fLambt[i-2]^2*g03[i-1]*lattice[i-1]*lattice[i])/
    ((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])) 
    + (b[i-1]*b[i]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*b[i-1]*b[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]
    *lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*a[i]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*b[i-1]*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*g111a[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*
    g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fNut[i-1]*fNut[i]*g111a[
    i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fNut[i-1]*fNut[i]*g111a[
    i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*g111a[i-1]*lattice[i-1]*
    lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])) 
    - (b[i-1]*b[i]*dNk[i-2]*fLambt[i-2]*g111a[i-1]*lattice[i-1]*lattice[i]
    )/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])) - 
    (a[i]*c[i-1]*dNk[i-2]*fLambt[i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/
    (2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + fLambt[i-2]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (5*a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (5*a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (5*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (5*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[
    i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-
    1]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (3*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (3*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (3*a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (7*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (3*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (3*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]^2*g111b[
    i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^2*fNut[i-2]*fNut[i-1]
    *g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-2]
    *g111b[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*g111b[i-
    1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (b[i-1]*b[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]*g111b[i-1]*
    lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]^2*
    g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*
    fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*dNk[i-1]*fLambt[i-2]*fLambt[i]*g111b[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*dNk[i]*fLambt[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *
    fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[i-2]*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*b[i-1]*b[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*
    g111b[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111b[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*dRk[i-2]*fDt[i]*fLambt[i]^2*g111b[i-1]*lattice[i-1]*
    lattice[i])/(2. *(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1]*fNut[i] + 
    fLambt[i-1]*fNut[i]^2)) - 
    (7*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-
    1]^2*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g12[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i]*c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^
    2*g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (a[i]*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g12[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i]*c[i-1]*dNk[i-1]*fLambt[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/(
    2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*g12[i-1]*lattice[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]^2*g12[i-
    1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^2*fNut[i-2]*fNut[i-1]
    *g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[i-
    2]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*a[i]*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]*fNut[i-1]*g12[i-
    1]*lattice[i-1]*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g12[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i]*c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]^2*
    g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]*fNut[
    i-2]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i-1]*g12[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g12[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g12[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fNut[i-2]*
    fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i]*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fNut[i]*g12[
    i-1]*lattice[i-1]*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^
    2*fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]^2*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]
    ^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    *fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]
    ^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]^3*
    fNut[i-2]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]^3*
    fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (3*a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]
    ^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (5*a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-
    1]*fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]
    ^2*fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-
    1]^2*fNut[i]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (a[i]*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    ^2*fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (3*b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-
    1]*fLambt[i]^2*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (3*b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[
    i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    *fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]
    ^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-
    1]*fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]
    *fNut[i-2]*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (5*b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-
    1]^2*fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-
    1]^3*fNut[i]^2*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-
    1]^2*fNut[i]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-
    1]^2*fLambt[i]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-
    1]*fLambt[i]^2*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]
    ^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-
    1]*fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]
    ^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i]*
    fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]^2*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(4. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^
    2*fNut[i-2]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^
    2*fNut[i-1]^2*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]*
    fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[
    i-1]*fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-
    1]^2*fLambt[i]*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]
    *fNut[i-1]*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i]
    *fNut[i-1]*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-
    1]*fNut[i]^2*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-
    1]^2*fNut[i]^2*lattice[i-1]^2*lattice[i])/(fNut[i-2]*fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[
    i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]
    *fNut[i-2]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]
    *fLambt[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*c[i-1]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*c[i-1]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*c[i-1]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*c[i-1]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*c[i-1]*dNk[i-1]*fDt[i-2]*fLambt[i-1]*fLambt[i]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*c[i-1]*dNk[i-2]*fDt[i-1]*fLambt[i-1]*fLambt[i]*lattice[i-
    1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*c[i-1]*dNk[i]*fDt[i-2]*fLambt[i-1]*fLambt[i]*lattice[i-1]
    ^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*c[i-1]*dNk[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]*lattice[i-1]
    ^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*c[i-1]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i]*b[i-1]*c[i-1]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]
    ^2*fNut[i]*lattice[i-1]^2*lattice[i])/(4. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*b[i]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-
    1]*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*c[i-1]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*b[i]*c[i-1]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g111b[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g111b[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g111b[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g111b[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g111b[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i+2]*fLambt[i+1]^2*g03[i+1]*lattice[i]*lattice[i+1])/
    ((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*g111b[i+1]*lattice[i]*
    lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (4*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (4*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]^2*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (4*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+
    2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dNk[i+1]*fLambt[i+1]*g111b[i+1]*lattice[i]*lattice[i+1])/
    (2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (2*a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+2]*g111b[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*c[i+1]*dNk[i]*fLambt[i]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *
    fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])) - 
    (3*a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (7*a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+
    1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*g12[i+1]*
    lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[
    i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i]*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*g12[
    i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+
    2]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]
    ^2*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i+1]
    *g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]^2*g12[
    i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+1]*g12[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fNut[i+2]*g12[i+
    1]*lattice[i]*lattice[i+1])/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+1]*fNut[i+
    2]*g12[i+1]*lattice[i]*lattice[i+1])/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i]
    *lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+
    1]*fNut[i+2]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i]*fLambt[i+1]^2*g03[i]*lattice[i+1]^2)/((fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (5*a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*fNut[
    i+1]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+1]^
    2*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*fNut[i+1]^2*g111b[
    i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*g111b[i]
    *lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]^2*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*fNut[i+
    1]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*
    g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (7*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]^2*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[
    i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g111b[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[
    i+2]*g111b[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+
    2]^2*g111b[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]*g111b[i]*
    lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (3*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]*fNut[i+1]^2*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*c[i+1]*dNk[i]*fLambt[i+1]*fLambt[i+2]*g111b[i]*lattice[i+1]^
    2)/((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] 
    + fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111b[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]^2*dNk[i+2]*fLambt[i+2]*g111b[i]*lattice[i+1]^2)/(2. *fNut[i+
    2]*(fLambt[i+2]*fNut[i] + fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] 
    + fLambt[i+1]*fNut[i+2])) + 
    (3*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fNut[i+2]*
    g111b[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*c[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fNut[i+1]*fNut[
    i+2]*g111b[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*c[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]*
    g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fNut[i]*fNut[i+2]*g12[
    i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fNut[i+2]*
    g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^
    2*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]
    ^2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+2]*g12[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]
    *fNut[i]*fNut[i+2]*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[
    i+2]^2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*a[i+1]*c[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]
    ^2*g12[i]*lattice[i+1]^2)/(4. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*c[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*g12[i]*lattice[i+1]^2)/(
    2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*c[i+1]*dNk[i+1]*fLambt[i+1]*g12[i]*lattice[i+1]^2)/(2. *fNut[i+
    1]*(fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i+1] 
    + fLambt[i+1]*fNut[i+2])) + 
    (3*a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]^2*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*
    fNut[i]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]*
    fNut[i]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]
    ^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]^2*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]
    *fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^3*
    fNut[i]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]
    *fNut[i]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^
    2*fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*
    fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^
    2*fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(fNut[i]*fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]^2*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]^2*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^
    2*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^
    2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]^2*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]^2*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]^2*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (5*a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]^2*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+2]*
    fNut[i+1]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*
    fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]^2*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]
    *lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]*
    fNut[i+1]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*c[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+2]*lattice[i]
    *lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*c[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+2]*lattice[i]
    *lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]^2*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*lattice[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]^2*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*lattice[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*c[i+1]*dNk[i+1]*fDt[i]*fLambt[i+1]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*c[i+1]*dNk[i]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*lattice[
    i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]^2*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+2]*lattice[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]^2*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]*lattice[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]^2*dNk[i+2]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]^2*dNk[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(4. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*c[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*c[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*c[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])))
end


function get_rhs_g03(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)

    (-((b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*
    g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2)) - 
    (2*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[i]
    *g03[i-1]*g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111a[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*
    g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i-1]*g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111a[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g03[i-1]*g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*g111a[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g03[i-1]*g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i-1]*g111a[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111a[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-
    1]*g111a[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g111a[i-1]*g111a[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i-1]*g111a[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*g111a[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g111a[i-1]*g111a[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i-1]*g111a[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-1]*g111a[i-1]*g111a[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[
    i-1]*g111a[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i]*g111a[i-1]*g111a[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*fNut[i]*g111a[i-1]*g111a[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*fNut[i]*
    g111a[i-1]*g111a[i]*lattice[i-1])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i-1]*g111a[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*fNut[i]^
    2*g111a[i-1]*g111a[i]*lattice[i-1])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-1]*g111a[i]*
    lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]*g111a[i]*
    lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*g111b[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g03[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i]*g111b[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g03[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*g111b[
    i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g03[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i]*g111b[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*g111b[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g03[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i]*g111b[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    g111a[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[
    i]*g111b[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*g111a[i]
    *g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i]*g111a[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g111a[i]*g111b[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i]*g111b[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g111a[i]*g111b[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[
    i]*g111b[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*g111a[
    i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i]*g111a[i]*g111b[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*fNut[i]*
    g111a[i]*g111b[i-1]*lattice[i-1])/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*fNut[i]*g111a[i]*g111b[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*fNut[i]^
    2*g111a[i]*g111b[i-1]*lattice[i-1])/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i]*g111b[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*g111a[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[
    i]*g111b[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*g111a[i]
    *g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i]*
    g111a[i]*g111b[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g111a[i]*g111b[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i]*g111b[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i-1]*fLambt[i-2]*fLambt[i]*g111a[i]*g111b[i-1]*lattice[i-
    1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111a[i]*g111b[i-1]*
    lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i]*g111b[i-1]*
    lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*g111b[i]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111b[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*
    g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111b[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g03[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*g111b[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g03[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111b[
    i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-
    1]*g111b[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g111a[i-1]*g111b[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i-1]*g111b[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g111a[i-1]*g111b[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g111a[i-1]*g111b[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i-1]*g111b[i]*lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-1]*g111a[i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[
    i-1]*g111b[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i]*g111a[i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*fNut[i]*g111a[i-1]*g111b[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*fNut[i]*
    g111a[i-1]*g111b[i]*lattice[i-1])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i-1]*g111b[i]*lattice[i-1])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*fNut[i]^
    2*g111a[i-1]*g111b[i]*lattice[i-1])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-1]*g111b[i]*
    lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]*g111b[i]*
    lattice[i-1])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (3*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    g111b[i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*g111b[i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[i-1]*
    g111b[i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-1]^2*
    g111b[i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i-
    1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-
    1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[
    i-1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-
    1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i-
    1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111b[i-1]*g111b[i]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i-
    1]*g111b[i]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i-1]*g111b[i]*
    lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111b[i-1]*g111b[i]*
    lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*g12[i-1]*
    lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g03[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i]*g12[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*g12[i-
    1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[
    i-1]*g03[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g03[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[
    i]*g03[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i]*g12[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i]*g12[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*c[i-1]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[
    i]*g03[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i]*g12[i-1]
    *lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i]
    *g12[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g111a[i]*g12[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i]*g12[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (3*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g111a[i]*g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[
    i]*g12[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (3*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i]*g111a[i]*g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*fNut[i]*
    g111a[i]*g12[i-1]*lattice[i-1])/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[
    i-2]*fNut[i]*g111a[i]*g12[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*fNut[i]^
    2*g111a[i]*g12[i-1]*lattice[i-1])/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i]*g12[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[
    i]*g12[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g111a[i]*g12[i-1]*lattice[i-1])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[
    i]*g12[i-1]*lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111a[i]*g12[i-1]*
    lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i]*g12[i-1]*
    lattice[i-1])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (5*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    g111b[i]*g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (5*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*g111b[i]*g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[i-1]*
    g111b[i]*g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-1]^2*
    g111b[i]*g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i]
    *g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    2]*fNut[i-1]*g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i]
    *g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g111b[i]
    *g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i]
    *g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i]
    *g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g111b[i]*
    g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111b[i]
    *g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111b[i]*g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111b[i]
    *g12[i-1]*lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g111b[i]*
    g12[i-1]*lattice[i-1])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i-1]*g111b[i]*g12[i-1]*lattice[i-1])
    /((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i]*g12[i-1]*
    lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111b[i]*g12[i-1]*
    lattice[i-1])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*
    g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-1]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g03[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]
    *fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g03[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g03[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]
    *g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-1]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g03[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i]*g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*fNut[i]*g03[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g03[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]
    *g03[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^
    2*fNut[i-1]*g03[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^
    2*g03[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]
    *fNut[i]*g03[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]
    *fNut[i-1]*fNut[i]*g03[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*
    fNut[i]*g03[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g03[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*
    fNut[i]^2*g03[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fLambt[i-1]*fLambt[i]*g03[i]*lattice[i-1]^2)/(
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g03[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g03[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*g111a[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*g111a[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*g111a[i]*lattice[i-1]^2)/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i]*
    fNut[i-1]*fNut[i]*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*g111a[i]*lattice[i-1]^2)/(fNut[i-2]*fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i]*g111a[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i]*g111a[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-1]*fDt[i-2]*fLambt[i-1]*fLambt[i]*g111a[i]*
    lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fDt[i-1]*fLambt[i-1]*fLambt[i]*g111a[i]*
    lattice[i-1]^2)/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*
    g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*
    g111a[i]*lattice[i-1]^2)/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[
    i-2]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[
    i-1]^2*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i]*fNut[
    i-1]^2*g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*g111b[i]*lattice[i-1]^2)/((fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111b[i]*lattice[i-1]^2)/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fNut[i-1]*fNut[i]*g111b[i]*lattice[i-1]^2)/(2. *fNut[i-2]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*g111b[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*g111b[i]*
    lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i]*fDt[i-2]*fLambt[i-1]*fLambt[i]*g111b[i]*lattice[
    i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]*g111b[i]*lattice[
    i-1]^2)/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111b[
    i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111b[
    i]*lattice[i-1]^2)/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i]*dRk[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (4*c[i]*dRk[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[i]
    *g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[i]
    *g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i-1]*
    g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (4*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i]*dRk[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[i]
    *g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*fNut[i]
    *g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i]*dRk[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g03[i-1]*g111a[
    i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*fNut[i-
    1]*g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i]*dRk[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*fNut[i]
    *g03[i-1]*g111a[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g03[i-1]*g111a[i-
    1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*g111a[i-1]^
    2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-1]*
    g111a[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i]*
    g111a[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    fNut[i]*g111a[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*g111a[i-
    1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    1]*g111a[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i]*
    g111a[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    fNut[i]*g111a[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-
    1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]*g111a[i-
    1]^2*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-
    1]*g111a[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-
    1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (5*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i]*
    g111a[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-
    1]*fNut[i]*g111a[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i]*fNut[i-1]^2*fNut[i]*
    g111a[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-
    1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fDt[i-2]*fLambt[i-2]^3*fLambt[i-1]*fNut[i-1]*fNut[i]^2*
    g111a[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dNk[i-2]*fLambt[i-1]*fLambt[i]*g111a[i-1]^2*lattice[i])/((
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-1]^2*lattice[i]
    )/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dNk[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]^2*lattice[i]
    )/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]
    *g111b[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    fNut[i]*g111a[i-1]*g111b[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-
    1]*g111b[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (3*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*g111a[i-1]*g111b[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-
    1]*g111b[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (3*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i]*
    g111a[i-1]*g111b[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*fNut[i]*
    g111a[i-1]*g111b[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-
    2]*fNut[i]*g111a[i-1]*g111b[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*fNut[i]^2*
    g111a[i-1]*g111b[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-
    1]*g111b[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-
    1]*g111b[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    fNut[i]*g111a[i-1]*g111b[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*g111a[i-
    1]*g111b[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111a[i-1]*g111b[i-1]*
    lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[i-1]*g111b[i-1]*
    lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (3*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*fNut[i-2]*
    g111a[i-1]*g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (3*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[i-1]*
    g111a[i-1]*g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[i-1]*
    g111a[i-1]*g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-1]^2*
    g111a[i-1]*g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]
    *g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-1]
    *g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-
    1]*g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-1]
    *g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*g111a[i-1]
    *g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[i-2]*
    fNut[i-1]*g111a[i-1]*g12[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*g111a[i-1]
    *g12[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111a[i-1]*g12[i-1]*
    lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-1]*g12[i-1]*
    lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (a[i]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (4*a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*g12[i+
    1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g12[i+1]*
    lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]*
    g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g12[i+1]*
    lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (4*a[i]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]*
    g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+2]
    *g03[i+1]*g12[i+1]*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i+1]*g12[i+1]
    *lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+1]
    *g12[i+1]*lattice[i])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*g12[i+1]*lattice[i])/(fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]
    *g12[i+1]*lattice[i])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+1]
    *g12[i+1]*lattice[i])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*g12[i+1]*lattice[i])/(fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]
    *g12[i+1]*lattice[i])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i+1]*
    g111a[i+1]*g12[i+1]*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+1]*
    g12[i+1]*lattice[i])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (3*a[i]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i+2]*
    g111a[i+1]*g12[i+1]*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*g12[i+1]*lattice[i])/(fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+2]*
    g111a[i+1]*g12[i+1]*lattice[i])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]*
    g12[i+1]*lattice[i])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]^2*
    g111a[i+1]*g12[i+1]*lattice[i])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111a[i+1]*g12[i+1]*
    lattice[i])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[i+1]*g12[i+1]*
    lattice[i])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[
    i+1]*g12[i+1]*lattice[i])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111b[i+1]*g12[i+1]*lattice[i])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+
    1]*g12[i+1]*lattice[i])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    g111b[i+1]*g12[i+1]*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i+
    1]*g12[i+1]*lattice[i])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+
    2]*g111b[i+1]*g12[i+1]*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*fNut[i+2]*
    g111b[i+1]*g12[i+1]*lattice[i])/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111b[i+1]*g12[i+1]*lattice[i])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+2]^2*
    g111b[i+1]*g12[i+1]*lattice[i])/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+
    1]*g12[i+1]*lattice[i])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i+1]
    *g12[i+1]*lattice[i])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111b[i+1]*g12[i+1]*lattice[i])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+1]
    *g12[i+1]*lattice[i])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111b[i+1]*g12[i+1]*
    lattice[i])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i+1]*g12[i+1]*
    lattice[i])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (5*a[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*g12[i+1]^2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+
    1]*g12[i+1]^2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*fNut[i+1]*
    g12[i+1]^2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+1]^2*
    g12[i+1]^2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[i+1]
    ^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[i+1]
    ^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*g12[i+1]
    ^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+
    1]*g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[i+1]
    ^2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[i+1]
    ^2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*g12[i+1]^
    2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[i+1]^
    2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g12[i+1]^2*lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[i+1]^
    2*lattice[i])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*g12[i+1]^2*
    lattice[i])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*dNk[i+2]*fLambt[i]*fLambt[i+1]*g12[i+1]^2*lattice[i])/((fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i+1]^2*lattice[i]
    )/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i+1]^2*lattice[i]
    )/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (5*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (5*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-2]^2*fNut[
    i-1]*g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-2]*fNut[i-
    1]^2*g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g03[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (5*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*fNut[i-2]^2*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*fNut[i-2]*
    fNut[i]^2*g03[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*
    fNut[i-2]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*fNut[
    i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]*
    g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[i-2]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[i]^2*fNut[
    i-2]*fNut[i-1]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[i-1]^2*
    g03[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]*g03[
    i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*fLambt[i]*
    fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*fLambt[i]*
    fNut[i-2]*fNut[i]*g03[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*fNut[i]^
    2*g03[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fLambt[i-2]*fLambt[i-1]*g03[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fLambt[i-2]*fLambt[i]*g03[i-1]*lattice[i-1]*
    lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g03[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g03[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g03[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g03[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-
    1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*fLambt[
    i]^2*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^3*fLambt[i]*
    fNut[i-2]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^3*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i]*fNut[i-
    1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-1]*g111a[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (2*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i]*fLambt[i-2]^3*fLambt[i-1]*
    fNut[i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]^2*
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i]*fNut[
    i-1]*fNut[i]*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^3*fLambt[i-1]*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]^2*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]^2*g111a[i-1]*lattice[i-1]*lattice[i])/(fNut[i-2]*fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (b[i-1]*c[i]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fDt[i-2]*fLambt[i-1]*fLambt[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-2]*fDt[i-1]*fLambt[i-1]*fLambt[i]*g111a[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fDt[i-2]*fLambt[i-1]*fLambt[i]*g111a[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]*g111a[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fDt[i-2]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-
    1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]*fNut[i-1]*g111a[i-
    1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-1]*fDt[i-2]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[
    i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dNk[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*fNut[i]*g111a[
    i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^3*fLambt[i]*fNut[i-
    2]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*
    fNut[i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] 
    + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^3*fNut[
    i-2]*fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-
    2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*g111b[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*
    fNut[i-2]^2*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g111b[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]
    *fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*g111b[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*g111b[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i]*g111b[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]*g111b[i-1]*lattice[
    i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i-
    1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g111b[i-
    1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (2*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]^3*fNut[i-
    2]^2*g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]^3*fNut[i-
    2]*fNut[i-1]*g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]^2*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^3*fLambt[i]*fNut[i-
    2]^2*g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (2*c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]^2*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^3*fNut[
    i-2]*fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    + (c[i-1]*c[i]*dRk[i-1]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) + 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]*
    fLambt[i]*g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2]
     + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]^2*
    fLambt[i]*fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/(fNut[i-1]*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    + (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    fLambt[i]^2*fNut[i-2]*g12[i-1]*lattice[i-1]*lattice[i])/((fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*fLambt[i]^2*fNut[
    i-2]^2*g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i]^2*fNut[
    i-1]*g12[i-1]*lattice[i-1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (c[i-1]*c[i]*dRk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*fLambt[i-1]^2*
    fNut[i]*g12[i-1]*lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*g12[i-1]*lattice[
    i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*g12[i-1]*lattice[
    i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-2]*fLambt[i]*g12[i-1]*lattice[i-
    1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]*g12[i-1]*lattice[i-
    1]*lattice[i])/(2. *(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dNk[i]*fDt[i-1]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g12[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (c[i-1]*c[i]*dNk[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]*fNut[i-2]*g12[i-1]*
    lattice[i-1]*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i-1]*fLambt[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]^2*fLambt[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]*
    fLambt[i]^3*fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i]^3*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *(fLambt[i-1]*
    fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]*fLambt[i]^2*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i-1]^2*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*fNut[i-1]*
    (fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + 
    fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i-1]*fLambt[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^3*
    fLambt[i]*fNut[i-2]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]^2*fLambt[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]*fLambt[i]^2*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]^3*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-1]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i-1]^2*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*fNut[i-1]*
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])^2) - 
    (2*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i-1]*fLambt[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (2*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]^2*fLambt[i]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[
    i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^3*
    fLambt[i]*fNut[i-1]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (2*b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*
    fLambt[i-1]*fLambt[i]^2*lattice[i-1]^2*lattice[i])/((fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]^2*
    fLambt[i]^2*fNut[i-2]*lattice[i-1]^2*lattice[i])/(fNut[i-1]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i]^2*fNut[i-1]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*(fLambt[i-
    1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*fNut[i]*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^3*
    fLambt[i-1]*fNut[i]*lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] 
    + fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) 
    - (b[i-1]*c[i-1]*c[i]*dRk[i-2]*fDt[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]^2*
    fLambt[i-1]^2*fNut[i]*lattice[i-1]^2*lattice[i])/(fNut[i-2]*fNut[i-1]*
    (fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])^2*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])^2*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i-1]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*fNut[i-1]*(fLambt[i-1]*fNut[
    i-2] + fLambt[i-2]*fNut[i-1])*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-2]*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-2]*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-2]*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i]*fDt[i-2]*fDt[i-1]*fLambt[i-1]*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-1]*fDt[i-2]*fDt[i]*fLambt[i-1]*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) + 
    (b[i-1]*c[i-1]*c[i]*dNk[i-2]*fDt[i-1]*fDt[i]*fLambt[i-1]*fLambt[i]*
    lattice[i-1]^2*lattice[i])/(2. *fNut[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])*fNut[i]*(fLambt[i]*fNut[i-2] + 
    fLambt[i-2]*fNut[i])*(fLambt[i]*fNut[i-1] + fLambt[i-1]*fNut[i])) - 
    (a[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i]*g111a[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g03[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*g111a[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i]*g111a[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g03[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g03[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g03[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*g111a[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i]*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g03[i]*g111a[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*g111a[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g111b[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g03[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g111b[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g03[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g03[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g111b[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g03[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g111b[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g111b[i]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g03[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g111b[i]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i+2]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*g111a[i+1]
    *g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i+1]*
    g111a[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+
    1]*g111b[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i+
    2]*g111a[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*g111b[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+
    1]*g111b[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*g111a[i+1]
    *g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i+
    1]*g111a[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+
    1]*g111b[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i+2]*
    g111a[i+1]*g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*g111b[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+
    1]*g111b[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*dRk[i]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*g111a[i+1]
    *g111b[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i+1]*
    g111a[i+1]*g111b[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111a[i+1]
    *g111b[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i+2]*
    g111a[i+1]*g111b[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*g111b[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+2]*
    g111a[i+1]*g111b[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+1]
    *g111b[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]^2*
    g111a[i+1]*g111b[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dNk[i]*fLambt[i+1]*fLambt[i+2]*g111a[i+1]*g111b[i]*lattice[i+
    1])/((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] 
    + fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111a[i+1]*g111b[i]*
    lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[i+1]*g111b[i]*
    lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i]*g111b[
    i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g03[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g03[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*g111b[i+
    1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g03[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i]*g111b[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+2]
    *g03[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[i+
    2]*g03[i]*g111b[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[i]*g111b[i+1]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]
    *g111b[i+1]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111b[i]*g111b[i+1]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i]
    *g111b[i+1]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]
    *g111b[i+1]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111b[i]*g111b[i+1]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i]
    *g111b[i+1]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i+1]*
    g111b[i]*g111b[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]*
    g111b[i+1]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (3*b[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i+2]*
    g111b[i]*g111b[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111b[i]*g111b[i+1]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+2]*
    g111b[i]*g111b[i+1]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i]*
    g111b[i+1]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]^2*
    g111b[i]*g111b[i+1]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111b[i]*g111b[i+1]*
    lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i]*g111b[i+1]*
    lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*dRk[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g12[
    i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g03[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g12[i]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*fNut[i+
    2]*g03[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+1]*fNut[
    i+2]*g03[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g12[i]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+
    1]*g03[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g12[i]
    *lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g03[i+1]*g12[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*b[i+1]*dRk[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*fNut[i+1]
    *g03[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dRk[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[i+1]*g12[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[
    i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111a[i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[
    i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*g111a[i+1]*g12[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[
    i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g111a[i+1]*g12[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*fNut[i+
    2]*g111a[i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g111a[i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+2]^2*
    g111a[i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[
    i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111a[i+
    1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111a[i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111a[i+
    1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111a[i+1]*g12[i]*
    lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[i+1]*g12[i]*
    lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*g111b[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[
    i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*g111b[i+
    1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g111b[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111b[i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[
    i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*g111b[i+1]*g12[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[
    i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*g111b[
    i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (5*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g111b[i+1]*g12[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*fNut[i+
    2]*g111b[i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g111b[i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+2]^2*
    g111b[i+1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (b[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[
    i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    g111b[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i+
    1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*g111b[i+1]
    *g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[i+2]*
    g111b[i+1]*g12[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[i]*
    fNut[i+2]*g111b[i+1]*g12[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g111b[i+
    1]*g12[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+2]*g111b[i+1]*g12[i]*lattice[i+1])
    /((fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111b[i+1]*g12[i]*
    lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (b[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[i+1]*g12[i]*
    lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (3*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g111b[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (3*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*g111b[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*fNut[i+
    1]*g111b[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+1]^2*
    g111b[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[
    i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g111b[i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[
    i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[
    i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g111b[i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[
    i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g111b[i]
    *g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g111b[i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g111b[i]
    *g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g111b[i]*g12[i+1]*
    lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g111b[i]*g12[i+1]*
    lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (3*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g12[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (3*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*g12[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*fNut[i+
    1]*g12[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[i+1]^2*
    g12[i]*g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[i]
    *g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i]*fNut[i+1]*g12[i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[i]
    *g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[i]
    *g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g12[i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[i]
    *g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*g12[i]*
    g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[i]*
    fNut[i+1]*g12[i]*g12[i+1]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g12[i]*
    g12[i+1]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (b[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[i]*g12[i+1]*
    lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i]*g12[i+1]*
    lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*g03[
    i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g03[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*g03[
    i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*
    g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i+1]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[i]^2*
    fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i]*fNut[i+
    2]^2*g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]*g03[
    i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]*g03[
    i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*fNut[
    i+1]*g03[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*g03[
    i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+2]*g03[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (5*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*fNut[
    i+1]*fNut[i+2]*g03[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^2*fNut[i+
    2]*g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*g03[
    i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]^2*
    g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*fNut[i+2]
    ^2*g03[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+2]*g03[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fLambt[i+1]*fLambt[i+2]*g03[i+1]*lattice[i]*
    lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g03[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g03[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g03[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g03[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i+
    2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[
    i]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+
    1]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*fNut[i+2]^
    2*g111a[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+
    2]^2*g111a[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+2]*g111a[i+1]*lattice[
    i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+2]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+1]*fDt[i]*fLambt[i+1]*fLambt[i+2]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*g111a[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[
    i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111a[
    i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i+1]^3*fLambt[i+2]*fNut[
    i]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^3*fNut[i+
    2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[
    i]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+
    1]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[
    i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]*
    fNut[i+2]*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^3*fLambt[i+1]*fNut[i+2]^
    2*g111b[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+
    2]^2*g111b[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+2]*g111b[i+1]*lattice[
    i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+2]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i+1]*fDt[i]*fLambt[i+1]*fLambt[i+2]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*g111b[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i+1]*fDt[i]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[
    i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*b[i+1]*dNk[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*fNut[i+2]*g111b[
    i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (2*a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]^2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[
    i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[
    i]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i+
    1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]*
    fNut[i]^2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^2*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^3*fNut[
    i]*fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[
    i+2]*fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[
    i+2]*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]*fNut[i+1]^
    2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^2*fNut[i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[
    i]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (2*a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+
    2]^2*fNut[i+1]*g12[i+1]*lattice[i]*lattice[i+1])/((fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+
    1]^2*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+
    2]*g12[i+1]*lattice[i]*lattice[i+1])/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*fNut[i+1]*
    fNut[i+2]*g12[i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i]^2*(fLambt[i+1]
    *fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*b[i+1]*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*g12[i+1]*lattice[i]
    *lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*g12[i+1]*lattice[
    i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+2]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i+2]*fDt[i]*fLambt[i+1]*fLambt[i+2]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*g12[i+1]*
    lattice[i]*lattice[i+1])/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[
    i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*a[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g12[
    i+1]*lattice[i]*lattice[i+1])/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i+
    1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i]*b[i+1]*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g12[i+
    1]*lattice[i]*lattice[i+1])/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (5*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+
    2]*fNut[i]*g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (5*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+1]*g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*fNut[i]^2*
    fNut[i+1]*g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[i]*fNut[
    i+1]^2*g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^
    2*fNut[i]*fNut[i+1]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]
    *g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]
    *fNut[i+1]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^
    2*g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^
    2*g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*fLambt[i+2]*
    fNut[i]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*fLambt[i+2]*
    fNut[i+1]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*fNut[i]^2*
    g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[i+2]^2*
    fNut[i]*fNut[i+1]*g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[i+1]^2*
    g03[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[i+2]*
    g03[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+1]*g03[i]*lattice[i+1]^2)/(
    (fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*g03[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*g03[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]*fNut[i+1]*g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*fNut[
    i+1]^2*g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i]
     + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]^2*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111b[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]^2*g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*fNut[i+1]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+2]*fNut[i+
    1]^2*g111b[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i+1]*g111b[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*fNut[
    i+1]^2*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*fNut[
    i+2]*g111b[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+2]*fLambt[i]^3*fLambt[i+1]*fNut[i+
    1]*fNut[i+2]*g111b[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(fLambt[i+1]*fNut[
    i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+1]*g111b[i]*lattice[
    i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*g111b[i]*
    lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i+2]*fDt[i]*fLambt[i+1]*fLambt[i+2]*g111b[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*g111b[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i+2]*fDt[i]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i+1]*b[i+1]*dNk[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]*fNut[i+1]*
    g111b[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]^3*
    fNut[i]^2*g12[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]^3*
    fNut[i]*fNut[i+1]*g12[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+2]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^3*fLambt[i+2]*
    fNut[i]^2*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (2*a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    fLambt[i+2]^2*fNut[i]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^3*
    fNut[i]*fNut[i+2]*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i+1]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) + 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]*
    fLambt[i+2]*g12[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]^2*
    fLambt[i+2]*fNut[i]*g12[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*fLambt[
    i+2]^2*fNut[i]*g12[i]*lattice[i+1]^2)/((fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*fLambt[i+2]^2*
    fNut[i]^2*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+2]^2*
    fNut[i+1]*g12[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i+1]*b[i+1]*dRk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*fLambt[i+1]^2*
    fNut[i+2]*g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i+1]*b[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+1]*g12[i]*lattice[
    i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*b[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*g12[i]*
    lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*b[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i]*fLambt[i+2]*g12[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*b[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]*g12[i]*
    lattice[i+1]^2)/(2. *(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*b[i+1]*dNk[i+2]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (a[i+1]*b[i+1]*dNk[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]*fNut[i]*
    g12[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    - (2*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+1]*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]^2*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*
    fLambt[i+2]^3*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+2]^3*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *(fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]*fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+2]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+1]^2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*fNut[i+1]*
    (fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+1]*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^3*
    fLambt[i+2]*fNut[i]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]^2*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]*fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]^3*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i+1]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+1]^2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*fNut[i+1]*
    (fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])^2) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+1]*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]^2*fLambt[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^3*
    fLambt[i+2]*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (2*a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*
    fLambt[i+1]*fLambt[i+2]^2*lattice[i]*lattice[i+1]^2)/((fLambt[i+1]*
    fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]^2*
    fLambt[i+2]^2*fNut[i]*lattice[i]*lattice[i+1]^2)/(fNut[i+1]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+2]^2*fNut[i+1]*lattice[i]*lattice[i+1]^2)/(fNut[i]*(fLambt[i+
    1]*fNut[i] + fLambt[i]*fNut[i+1])^2*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^3*
    fLambt[i+1]*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(2. *fNut[i]^2*(
    fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) - 
    (a[i]*a[i+1]*b[i+1]*dRk[i]*fDt[i]*fDt[i+1]*fDt[i+2]*fLambt[i]^2*
    fLambt[i+1]^2*fNut[i+2]*lattice[i]*lattice[i+1]^2)/(fNut[i]*fNut[i+1]*
    (fLambt[i+1]*fNut[i] + fLambt[i]*fNut[i+1])^2*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])^2*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])) + 
    (a[i]*a[i+1]*b[i+1]*dNk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+1]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*fNut[i+1]*(fLambt[i+1]*fNut[i] 
    + fLambt[i]*fNut[i+1])*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+2]*fDt[i]*fDt[i+1]*fLambt[i]*fLambt[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+1]*fDt[i]*fDt[i+2]*fLambt[i]*fLambt[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i]*fDt[i+1]*fDt[i+2]*fLambt[i]*fLambt[i+2]*
    lattice[i]*lattice[i+1]^2)/(2. *fNut[i]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+2]*fDt[i]*fDt[i+1]*fLambt[i+1]*fLambt[i+2]
    *lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i+1]*fDt[i]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]
    *lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])) 
    + (a[i]*a[i+1]*b[i+1]*dNk[i]*fDt[i+1]*fDt[i+2]*fLambt[i+1]*fLambt[i+2]
    *lattice[i]*lattice[i+1]^2)/(2. *fNut[i+1]*(fLambt[i+1]*fNut[i] + 
    fLambt[i]*fNut[i+1])*fNut[i+2]*(fLambt[i+2]*fNut[i] + 
    fLambt[i]*fNut[i+2])*(fLambt[i+2]*fNut[i+1] + fLambt[i+1]*fNut[i+2])))
end