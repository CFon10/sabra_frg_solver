
import CurveFit
include("reg.jl")
include("int_threept.jl")
include("int_fourpt.jl")
include("int_twopt.jl")


function extrapolate(f, lattice, param; mode = "std")
"""
This function is used to extrapolate the value of the running function
for values of momentam above the maximum one considered. The method
is power law extrapolation. Several method can be used. Std and fd use
loglog fits, whereas logDiff, leo_f and leo_gam only use the last two 
point to compute a finite difference.

the extrapolated function has 4 point more than the original one, as 
needed in the rest of the calculation

Args: 
    f::array || value of the running functions
    lattice::array || log lattice of momentum
    param::Parameters 
    mode::string

Output
    extrapolated_f::array
"""
    f_ext = zeros((param.n_shell+4))
    f_ext[3:end-2] = f

    if (mode == "std")
        lower_fit = CurveFit.linear_fit(lattice[1:5] .|> log, f[1:5] .|> abs .|> log)
        upper_fit = CurveFit.linear_fit(lattice[end-5:end] .|>log, f[end-5:end] .|> abs .|>log)
        if isnan(lower_fit[1])
            f_ext[1] = 0
            f_ext[2] = 0
        else
            f_ext[1] = exp(lower_fit[1] + lower_fit[2] * log(lattice[1]/param.lamb^2))
            f_ext[2] = exp(lower_fit[1] + lower_fit[2] * log(lattice[1]/param.lamb))
        end
        if isnan(upper_fit[1])
            f_ext[end] = 0
            f_ext[end-1] = 0
        else
            f_ext[end] = exp(upper_fit[1] + upper_fit[2] * log(lattice[end]*param.lamb^2))
            f_ext[end-1] = exp(upper_fit[1] + upper_fit[2] * log(lattice[end]*param.lamb))
        end
    end

    if (mode == "fd")
        upper_fit = CurveFit.linear_fit(lattice[end-5:end] .|>log, f[end-5:end] .|> abs .|>log)
        f_ext[1] = 0
        f_ext[2] = 0
        if isnan(upper_fit[1])
            f_ext[end] = 0
            f_ext[end-1] = 0
        else
            f_ext[end] = exp(upper_fit[1] + upper_fit[2] * log(lattice[end]*param.lamb^2))
            f_ext[end-1] = exp(upper_fit[1] + upper_fit[2] * log(lattice[end]*param.lamb))
        end
    end

    if mode == "logdiff"
        logdiff_lower = (log(abs(f[2]))-log(abs(f[1])))/(log(param.lamb))
        logdiff_upper = (log(abs(f[end]))-log(abs(f[end - 1])))/(log(param.lamb))
        if isnan(logdiff_lower)
            f_ext[1] = 0
            f_ext[2] = 0
        else
            f_ext[2] =  f[1]*exp(-log(param.lamb)*logdiff_lower)
            f_ext[1] =  f[1]*exp(-log(param.lamb^2)*logdiff_lower)
        end
        if isnan(logdiff_upper)
            f_ext[end-1] = 0
            f_ext[end] = 0
        else
            f_ext[end-1] =  f[end]*exp(log(param.lamb)*logdiff_upper)
            f_ext[end] =  f[end]*exp(log(param.lamb^2)*logdiff_upper)
        end


    end

    if mode == "debug"
        f_ext[1] = f[1]
        f_ext[2] = f[1]
        f_ext[end] = f[end]
        f_ext[end-1] = f[end]
    end

    if (mode == "leo_f")
        zetamax = 0
        zetamin = 0
        lamb = param.lamb
        if (f[end] > 0 && f[end-1] > 0)
            zetamax = (log(f[end])-log(f[end-1]))/log(param.lamb) 
        else
            zetamax = 0
        end
        f_ext[end] = f[end]*lamb^(2*zetamax)
        f_ext[end-1] = f[end]*lamb^zetamax

        if (f[1] > 0 && f[2] > 0)
            zetamax = (log(f[2])-log(f[1]))/log(param.lamb) 
        else
            zetamax = 0
        end
        f_ext[1] = f[1]*lamb^(-2*zetamax)
        f_ext[2] = f[1]*lamb^(-zetamax)
    end

    if (mode == "leo_gam")
        zetamax = 0
        zetamin = 0
        lamb = param.lamb
        if (f[end] > 0 && f[end-1] > 0)
            zetamax = (log(f[end])-log(f[end-1]))/log(param.lamb) 
        elseif (f[end] < 0 && f[end-1] < 0)
            zetamax = (log(-f[end])-log(-f[end-1]))/log(param.lamb)
        else
            zetamax = 0
        end
        f_ext[end] = f[end]*lamb^(2*zetamax)
        f_ext[end-1] = f[end]*lamb^zetamax

        if (f[1] > 0 && f[2] > 0)
            zetamax = (log(f[2])-log(f[1]))/log(param.lamb) 
        elseif (f[1] < 0 && f[2] < 0)
            zetamax = (log(-f[2])-log(-f[1]))/log(param.lamb)
        else
            zetamax = 0
        end
        f_ext[1] = f[1]*lamb^(-2*zetamax)
        f_ext[2] = f[1]*lamb^(-zetamax)
    end

    return f_ext
end


function get_rhs(f0, s, lattice, param)
"""
This function computes the right hand side of the equation

Args: 
    f0::array || value of the running functions
    s::float || value of the RG time
    lattice::array || log lattice of momentum
    param::Parameters 

Output
    rhs::array
"""
    rhs_tot = zeros(size(f0))
    (f_nu, f_D, f_lamb, c, a, b, g111a, g111b, g12, g03, g22b, g22a, g31, g13, g04) = split_views(f0, param.n_shell, 15)
    (rhs_nu, rhs_D, rhs_lamb, rhs_c, rhs_a, rhs_b, rhs_g111a, rhs_g111b, rhs_g12, rhs_g03, rhs_g22b, rhs_g22a, rhs_g31, rhs_g13, rhs_g04) = split_views(rhs_tot, param.n_shell, 15)

    
    for i in 1:param.n_shell
        if f_nu[i]<0
            f_nu[i] = 0
        end
        if f_lamb[i]<0
            f_lamb[i] = 0
        end
    end 

    kappa = (lattice[1] / param.lamb^2) * exp(s)
    ad_lattice2 = zeros(param.n_shell + 4)
    ad_lattice2[1] = (lattice[1]/kappa/param.lamb^2).^2
    ad_lattice2[2] = (lattice[1]/kappa/param.lamb).^2
    ad_lattice2[end-1] = (lattice[end]/kappa*param.lamb).^2
    ad_lattice2[end] = (lattice[end]/kappa*param.lamb^2).^2
    ad_lattice2[3:end-2] = (lattice/kappa).^2

    lattice_ext = zeros(param.n_shell + 4)
    lattice_ext[3:end-2] = lattice
    lattice_ext[1] = (lattice[1]/param.lamb^2)
    lattice_ext[2] = (lattice[1]/param.lamb)
    lattice_ext[end-1] = (lattice[end]*param.lamb)
    lattice_ext[end] = (lattice[end]*param.lamb^2)

    f_nu_ext = extrapolate(f_nu, lattice, param, mode = "leo_f") .+ param.norm^4*(param.alpha*param.nu.*(ad_lattice2 .|> rk))./kappa^2
    f_D_ext = extrapolate(f_D, lattice, param, mode = "leo_gam") .+ param.norm^2*(param.beta*param.D.*(ad_lattice2 .|> rk))./kappa^2
    #f_nu_ext = extrapolate(f_nu, lattice, param, mode = "std") .+ param.norm^2*(param.alpha*param.nu.*ad_lattice2.*(ad_lattice2 .|> rk))
    #f_D_ext = extrapolate(f_D, lattice, param, mode = "fd") .+ (param.beta*param.D.*ad_lattice2.* (ad_lattice2 .|> rk))
    f_lamb_ext = extrapolate(f_lamb, lattice, param, mode = "leo_f")

    c_ext = extrapolate(c, lattice, param, mode = "leo_gam") 
    a_ext = extrapolate(a, lattice, param, mode = "leo_gam") 
    b_ext = extrapolate(b, lattice, param, mode = "leo_gam") 

    g111a_ext = extrapolate(g111a, lattice, param, mode = "leo_gam") 
    g111b_ext = extrapolate(g111b, lattice, param, mode = "leo_gam") 
    g12_ext = extrapolate(g12, lattice, param, mode = "leo_gam") 
    g03_ext = extrapolate(g03, lattice, param, mode = "leo_gam") 

    g22b_ext = extrapolate(g22b, lattice, param, mode = "leo_gam")
    g22a_ext = extrapolate(g22a, lattice, param, mode = "leo_gam")
    g31_ext = extrapolate(g31, lattice, param, mode = "leo_gam")
    g13_ext = extrapolate(g13, lattice, param, mode = "leo_gam")
    g04_ext = extrapolate(g04, lattice, param, mode = "leo_gam")

    #print(f_nu_ext)

    drk = - 2*param.norm^4*param.alpha*param.nu.*((ad_lattice2 .|> rk)+ad_lattice2.^2 .*(ad_lattice2 .|> dRk))./kappa^2
    dnk = - param.norm^2*param.beta*param.D .*(2 .*(ad_lattice2 .|> rk)+2*ad_lattice2.^2 .*(ad_lattice2 .|> dRk))./kappa^2

    #drk = - ad_lattice2.^2*param.norm^2*param.alpha*param.nu.*(2*(ad_lattice2 .|> rk)+2*ad_lattice2.*(ad_lattice2 .|> dRk))
    #dnk = - ad_lattice2.^2*param.beta*param.D.*(2*(ad_lattice2 .|> rk)+2*ad_lattice2.*(ad_lattice2 .|> dRk))

    for i in 1:param.n_shell
        rhs_nu[i] = get_rhs_nu(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)
 #      rhs_nu[i] = 0
        rhs_D[i] = get_rhs_D(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)
#       rhs_D[i] = 0.
        rhs_lamb[i] = get_rhs_lamb(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)
        #rhs_lamb[i] = 0
        rhs_c[i] = get_rhs_C(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)
#       rhs_c[i] = 0
        rhs_a[i] = get_rhs_A(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)
#       rhs_a[i] = 0.
        rhs_b[i] = get_rhs_B(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)

        rhs_g111a[i] = get_rhs_g111a(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)
        #       rhs_b[i] = 0
        rhs_g111b[i] = get_rhs_g111b(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)

##        rhs_g12[i] = get_rhs_g12(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)

#        rhs_g03[i] = get_rhs_g03(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, drk, dnk , param)

##        rhs_g22b[i] = get_rhs_g22b(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)
        #       rhs_b[i] = 0
##        rhs_g22a[i] = get_rhs_g22a(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)

#        rhs_g31[i] = get_rhs_g31(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)

#        rhs_g13[i] = get_rhs_g13(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)

#        rhs_g04[i] = get_rhs_g04(i+2, lattice_ext, f_nu_ext, f_D_ext, f_lamb_ext, c_ext, a_ext, b_ext, g111a_ext, g111b_ext, g12_ext, g03_ext, g22b_ext, g22a_ext, g31_ext, g13_ext, g04_ext, drk, dnk , param)
    end

     for i in 1:15*param.n_shell
        if isnan(rhs_tot[i])
            rhs_tot[i] = 0
        end
    end 

    return rhs_tot
end



function split_views(f0, part_size, nb_parts)
    return [@view f0[(i-1)*part_size+1:i*part_size] for i in 1:nb_parts]
end

