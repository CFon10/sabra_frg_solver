
using DelimitedFiles
using Printf

function Adaptative_Euler(rhs_function, f0, interval, max_step; adaptative_step_min = 1E-8, save_folder = "", save_nb = 100, args = ())
"""
This function is an implementation of the Euler scheme: x(t+dt) = x(t) + f(x,t)*dt
It feature a variable step depending on the value of the non-linear term

Args: 
    rhs_function::function || takes the form f(x,t; args)
    f0::array || intial value of x
    interval::(float, float) || tuple containing the starting time and end time
    max_step::float || maximum value of the time step

Kwargs:
    adaptative_step_min::float || minimum value of the time step 
    save_folder::string || name of the folder containing the output data
    save_nb::int || number of time point saved
    args::iterable || argument to be passed down to f

Output
    (x::array, t::array)
"""
    f = f0
    s = interval[1]
    sign_ds = (interval[2]-interval[1])/abs(interval[2]-interval[1])
    n_iteration = 0

    ds_save = (interval[2]-interval[1])/save_nb
    s_last_save = s 

    while abs(s) < abs(interval[2])
        rhs = rhs_function(f, s, args...)
        dlogf_array = filter(x -> isfinite(x), array(rhs./f .|> abs)).|> abs
        if isempty(dlogf_array)
            dlogf = 0
        else
            dlogf = maximum(dlogf_array)
        end
        ds = sign_ds*get_step(dlogf, abs(max_step), adaptative_step_min)
        f = f .+ (ds.*rhs)
        s = s + ds
        n_iteration += 1

        #print('\n')
        #print(f)
        #print(s)
        #print(ds)

        if abs(s - (s_last_save)) > abs(ds_save)
            save_data(f, rhs, s, dlogf, save_folder)
            @printf("s = %.2e   dlogf = %.3e    n_iter = %.1e\n", s, dlogf, n_iteration)
            s_last_save = s
            n_iteration = 0
        end
    end
    return (f, s)
end

function save_data(f, df, s, dlogf, save_folder)
    open(save_folder*"/f.dat", "a") do io_f
        writedlm(io_f, [f], '\t')
    end
    open(save_folder*"/df.dat", "a") do io_f
        writedlm(io_f, [df], '\t')
    end
    open(save_folder*"/s.dat", "a") do io_s
        writedlm(io_s, [[s, dlogf]], '\t')
    end
end

function array(x)
    if isa(x, Array)
        return x
    else
        return [x]
    end
end

function get_step(dlogf, ds, adaptative_step_min)
    return max(min(0.01/dlogf, ds), adaptative_step_min)
end

function f(x,S)
    -x
end

