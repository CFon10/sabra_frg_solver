
function save_struct(filename::AbstractString, s)
    io = open(filename, "w")
    dump(io, s)
    close(io)
end

function save_init(save_folder::AbstractString, f_init::Array, lattice::Array)
    io_f = open(save_folder*"f.dat", "w")
    io_lattice = open(save_folder*"lattice.dat", "w")

    write(io_f, join(f_init, '\t')*'\n')
    write(io_lattice, join(lattice, '\t'))

    close(io_f)
    close(io_lattice)
end