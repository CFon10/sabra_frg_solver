include("reg.jl")

"""
These functions all compute the flow of the two point vertices. They
share the same prototype
These expression are the output of a Mathematica code make the formal
calculations from the Wetterich equation

Args:
i::int || index of the momenta
lattice::Arrays || lattice of momenta
fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, gamma22b, gamma22a, gamma31, 
gamma13, gamma04, dRk, dNk::Arrays || running function and regulator
param::Parameters
"""

function get_rhs_D(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, gamma22b, gamma22a, gamma31, gamma13, gamma04, dRk, dNk , param)
    n = i
    lamb = param.lamb


    return (((-2*c[i-1]*(fNut[i-1]^2*(2*(dRk[i-1]*fLambt[i-2] + 
    dRk[i-2]*fLambt[i-1])*fNut[i-2]^2*g03[i-1] + 
    (fNut[i-2]*(dRk[i-1]*fDt[i-2]*fLambt[i-2] + 
    fLambt[i-1]*(2*dRk[i-2]*fDt[i-2] - dNk[i-2]*fNut[i-2])) + 
    fLambt[i-2]*(dRk[i-2]*fDt[i-2] - 
    dNk[i-2]*fNut[i-2])*fNut[i-1])*g111a[i-1]) + 
    fNut[i-2]^2*(dRk[i-1]*fDt[i-1]*(fLambt[i-1]*fNut[i-2] + 
    2*fLambt[i-2]*fNut[i-1]) + fNut[i-1]*(dRk[i-2]*fDt[i-1]*fLambt[i-1] - 
    dNk[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])))*g111b[i-1])*lattice[i-1])/(fNut[i-2]^2*fNut[
    i-1]^2*(fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2) - 
    (c[i-1]^2*(dRk[i-1]*fDt[i-2]*fDt[i-1]*fNut[i-2]*(fLambt[i-1]*fNut[i-2]
     + 2*fLambt[i-2]*fNut[i-1]) + fNut[i-1]*(-((dNk[i-1]*fDt[i-2] + 
    dNk[i-2]*fDt[i-1])*fNut[i-2]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])) + 
    dRk[i-2]*fDt[i-2]*fDt[i-1]*(2*fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1])))*lattice[i-1]^2)/(fNut[i-2]^2*fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2) - 
    ((2*b[i]*(dRk[i+1]*fNut[i-1]*(fLambt[i-1]*fNut[i+1]^2*(2*fNut[i-1]*
    g03[i] + fDt[i-1]*g111a[i]) - 
    fDt[i+1]*fNut[i-1]*(fLambt[i+1]*fNut[i-1] + 
    2*fLambt[i-1]*fNut[i+1])*g12[i]) + 
    fNut[i+1]*(fNut[i-1]*(fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])*(-(dNk[i-1]*fNut[i+1]*g111a[i]) + 
    dNk[i+1]*fNut[i-1]*g12[i]) + 
    dRk[i-1]*(fDt[i-1]*fLambt[i-1]*fNut[i+1]^2*g111a[i] + 
    fLambt[i+1]*fNut[i-1]*(2*fNut[i+1]*(fNut[i-1]*g03[i] + 
    fDt[i-1]*g111a[i]) - 
    fDt[i+1]*fNut[i-1]*g12[i]))))*lattice[i])/(fNut[i-1]^2*(fLambt[i+1]*
    fNut[i-1] + fLambt[i-1]*fNut[i+1])^2) + 
    (b[i]^2*(dRk[i+1]*fDt[i-1]*fDt[i+1]*fNut[i-1]*(fLambt[i+1]*fNut[i-1] 
    + 2*fLambt[i-1]*fNut[i+1]) + fNut[i+1]*(-((dNk[i+1]*fDt[i-1] + 
    dNk[i-1]*fDt[i+1])*fNut[i-1]*(fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])) + 
    dRk[i-1]*fDt[i-1]*fDt[i+1]*(2*fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])))*lattice[i]^2)/(fNut[i-1]^2*(fLambt[i+1]*fNut[
    i-1] + fLambt[i-1]*fNut[i+1])^2) + 
    (a[i+1]*lattice[i+1]*(dRk[i+2]*fNut[i+1]*(2*fLambt[i+1]*fNut[i+2]^2*(
    2*fNut[i+1]*g03[i+1] + fDt[i+1]*g111b[i+1]) - 
    2*fDt[i+2]*fNut[i+1]*(fLambt[i+2]*fNut[i+1] + 
    2*fLambt[i+1]*fNut[i+2])*g12[i+1] + 
    a[i+1]*fDt[i+1]*fDt[i+2]*(fLambt[i+2]*fNut[i+1] + 
    2*fLambt[i+1]*fNut[i+2])*lattice[i+1]) + 
    fNut[i+2]*(-(fNut[i+1]*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2])*(2*dNk[i+1]*fNut[i+2]*g111b[i+1] - 
    2*dNk[i+2]*fNut[i+1]*g12[i+1] + a[i+1]*(dNk[i+2]*fDt[i+1] + 
    dNk[i+1]*fDt[i+2])*lattice[i+1])) + 
    dRk[i+1]*(fDt[i+1]*fLambt[i+1]*fNut[i+2]*(2*fNut[i+2]*g111b[i+1] + 
    a[i+1]*fDt[i+2]*lattice[i+1]) + 
    2*fLambt[i+2]*fNut[i+1]*(fNut[i+1]*(2*fNut[i+2]*g03[i+1] - 
    fDt[i+2]*g12[i+1]) + fDt[i+1]*(2*fNut[i+2]*g111b[i+1] + 
    a[i+1]*fDt[i+2]*lattice[i+1]))))))/(fNut[i+2]^2*(fLambt[i+2]*fNut[i+1]
     + fLambt[i+1]*fNut[i+2])^2))/fNut[i+1]^2)/2. - ((-(dRk[i]*fDt[i]) + 
    dNk[i]*fNut[i])*gamma22a[i])/(2. *fLambt[i]*fNut[i]^2))
end


function get_rhs_nu(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, gamma22b, gamma22a, gamma31, gamma13, gamma04, dRk, dNk , param)
    n = i
    lamb = param.lamb


    return (((c[i-1]*lattice[i-1]*(-2*(dRk[i-1]*fLambt[i-2] + 
    dRk[i-2]*fLambt[i-1])*fNut[i-2]^2*fNut[i-1]^2*g12[i-1] + 
    (b[i-1]*fNut[i-1]^2*(fNut[i-2]*(dRk[i-1]*fDt[i-2]*fLambt[i-2] + 
    fLambt[i-1]*(2*dRk[i-2]*fDt[i-2] - dNk[i-2]*fNut[i-2])) + 
    fLambt[i-2]*(dRk[i-2]*fDt[i-2] - dNk[i-2]*fNut[i-2])*fNut[i-1]) + 
    a[i-1]*fNut[i-2]^2*(dRk[i-1]*fDt[i-1]*(fLambt[i-1]*fNut[i-2] + 
    2*fLambt[i-2]*fNut[i-1]) + fNut[i-1]*(dRk[i-2]*fDt[i-1]*fLambt[i-1] - 
    dNk[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1]))))*lattice[i-1]))/(fNut[i-2]^2*fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^2) + 
    ((b[i]*lattice[i]*(dRk[i+1]*fNut[i-1]*(a[i]*fDt[i+1]*fLambt[i+1]*fNut[
    i-1]^2*lattice[i] + 
    fLambt[i-1]*fNut[i+1]*(c[i]*fDt[i-1]*fNut[i+1]*lattice[i] + 
    2*fNut[i-1]*(fNut[i+1]*g111b[i] + a[i]*fDt[i+1]*lattice[i]))) + 
    fNut[i+1]*(-(fNut[i-1]*(a[i]*dNk[i+1]*fNut[i-1] + 
    c[i]*dNk[i-1]*fNut[i+1])*(fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])*lattice[i]) + 
    dRk[i-1]*(c[i]*fDt[i-1]*fLambt[i-1]*fNut[i+1]^2*lattice[i] + 
    fLambt[i+1]*fNut[i-1]*(2*c[i]*fDt[i-1]*fNut[i+1]*lattice[i] + 
    fNut[i-1]*(2*fNut[i+1]*g111b[i] + 
    a[i]*fDt[i+1]*lattice[i]))))))/(fNut[i-1]^2*(fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])^2) + 
    (a[i+1]*lattice[i+1]*(2*(dRk[i+2]*fLambt[i+1] + 
    dRk[i+1]*fLambt[i+2])*fNut[i+1]^2*fNut[i+2]^2*g111a[i+1] + 
    (c[i+1]*fNut[i+2]^2*(fNut[i+1]*(dRk[i+2]*fDt[i+1]*fLambt[i+1] + 
    fLambt[i+2]*(2*dRk[i+1]*fDt[i+1] - dNk[i+1]*fNut[i+1])) + 
    fLambt[i+1]*(dRk[i+1]*fDt[i+1] - dNk[i+1]*fNut[i+1])*fNut[i+2]) + 
    b[i+1]*fNut[i+1]^2*(dRk[i+2]*fDt[i+2]*(fLambt[i+2]*fNut[i+1] + 
    2*fLambt[i+1]*fNut[i+2]) + fNut[i+2]*(dRk[i+1]*fDt[i+2]*fLambt[i+2] - 
    dNk[i+2]*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2]))))*lattice[i+1]))/(fNut[i+2]^2*(fLambt[i+2]*
    fNut[i+1] + fLambt[i+1]*fNut[i+2])^2))/fNut[i+1]^2)/2. + 
    ((-(dRk[i]*fDt[i]) + 
    dNk[i]*fNut[i])*0)/(2. *fLambt[i]*fNut[i]^2))
end

function get_rhs_lamb(i, lattice, fNut, fDt, fLambt, c, a, b, g111a, g111b, g12, g03, dRk, dNk , param)
    n = i
    lamb = param.lamb
    
    return (((c[i-1]*fLambt[i-2]*fLambt[i-1]*lattice[i-1]*(4*(dRk[i-1]*fLambt[i-
    2] + dRk[i-2]*fLambt[i-1])*fNut[i-2]^2*fNut[i-1]^2*g12[i-1] + 
    (b[i-1]*fNut[i-1]^2*(fNut[i-2]*(-2*dRk[i-1]*fDt[i-2]*fLambt[i-2] + 
    fLambt[i-1]*(-3*dRk[i-2]*fDt[i-2] + dNk[i-2]*fNut[i-2])) + 
    fLambt[i-2]*(-(dRk[i-2]*fDt[i-2]) + dNk[i-2]*fNut[i-2])*fNut[i-1]) + 
    a[i-1]*fNut[i-2]^2*(-(dRk[i-1]*fDt[i-1]*(fLambt[i-1]*fNut[i-2] + 
    3*fLambt[i-2]*fNut[i-1])) + 
    fNut[i-1]*(-2*dRk[i-2]*fDt[i-1]*fLambt[i-1] + 
    dNk[i-1]*(fLambt[i-1]*fNut[i-2] + 
    fLambt[i-2]*fNut[i-1]))))*lattice[i-1]))/(fNut[i-2]^2*fNut[i-1]^2*(
    fLambt[i-1]*fNut[i-2] + fLambt[i-2]*fNut[i-1])^3) - 
    (fLambt[i+1]*((b[i]*fLambt[i-1]*lattice[i]*(dRk[i+1]*fNut[i-1]*(a[i]*
    fDt[i+1]*fLambt[i+1]*fNut[i-1]^2*lattice[i] + 
    fLambt[i-1]*fNut[i+1]*(2*c[i]*fDt[i-1]*fNut[i+1]*lattice[i] + 
    fNut[i-1]*(4*fNut[i+1]*g111b[i] + 3*a[i]*fDt[i+1]*lattice[i]))) + 
    fNut[i+1]*(-(fNut[i-1]*(a[i]*dNk[i+1]*fNut[i-1] + 
    c[i]*dNk[i-1]*fNut[i+1])*(fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])*lattice[i]) + 
    dRk[i-1]*(c[i]*fDt[i-1]*fLambt[i-1]*fNut[i+1]^2*lattice[i] + 
    fLambt[i+1]*fNut[i-1]*(3*c[i]*fDt[i-1]*fNut[i+1]*lattice[i] + 
    2*fNut[i-1]*(2*fNut[i+1]*g111b[i] + 
    a[i]*fDt[i+1]*lattice[i]))))))/(fNut[i-1]^2*(fLambt[i+1]*fNut[i-1] + 
    fLambt[i-1]*fNut[i+1])^3) + 
    (a[i+1]*fLambt[i+2]*lattice[i+1]*(4*(dRk[i+2]*fLambt[i+1] + 
    dRk[i+1]*fLambt[i+2])*fNut[i+1]^2*fNut[i+2]^2*g111a[i+1] + 
    (c[i+1]*fNut[i+2]^2*(fNut[i+1]*(2*dRk[i+2]*fDt[i+1]*fLambt[i+1] + 
    fLambt[i+2]*(3*dRk[i+1]*fDt[i+1] - dNk[i+1]*fNut[i+1])) + 
    fLambt[i+1]*(dRk[i+1]*fDt[i+1] - dNk[i+1]*fNut[i+1])*fNut[i+2]) + 
    b[i+1]*fNut[i+1]^2*(dRk[i+2]*fDt[i+2]*(fLambt[i+2]*fNut[i+1] + 
    3*fLambt[i+1]*fNut[i+2]) - 
    fNut[i+2]*(-2*dRk[i+1]*fDt[i+2]*fLambt[i+2] + 
    dNk[i+2]*(fLambt[i+2]*fNut[i+1] + 
    fLambt[i+1]*fNut[i+2]))))*lattice[i+1]))/(fNut[i+2]^2*(fLambt[i+2]*
    fNut[i+1] + fLambt[i+1]*fNut[i+2])^3)))/fNut[i+1]^2)/2.)
end